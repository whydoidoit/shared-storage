module.exports = function (config) {
    config.set({
        frameworks: ['mocha', 'chai'],
        files: [
            {
                pattern: 'test-context.js'
            },
            {
                pattern: "shared.html",
                included: false,
                served: true
            }
        ],
        reporters: ['progress'],
        preprocessors: {
            "test-context.js": ["webpack"]
        },
        webpack: {
            mode: "development",
            module: {
                rules: [{test: /\.js/, exclude: /node_modules/, loader: "babel-loader"}]
            },
            watch: true
        },
        webpackServer: {
            noInfo: true,
            contentBase: __dirname
        },
        proxies: {
            "/shared.html": "/base/shared.html"
        },
        port: 9876,  // karma web server port
        colors: true,
        basePath: ".",
        logLevel: config.LOG_INFO,
        browsers: ['ChromeHeadless'],
        autoWatch: false,
        // singleRun: false, // Karma captures browsers, runs the tests and exits
        concurrency: Infinity
    });
};
