# Shared Storage

Sometimes you may have an application which needs to share local storage between multiple URLs.  For instance you might have an application that is accessed from different domains but shares a user identity.  This module helps you share basic localStorage data between multiple applications by simply using an `iframe` which is embedded pointing to a well known URL that contains a simple process for getting and setting localStorage.

### Usage

`npm install --save https://bitbucket.org/whydoidoit/shared-storage`

In the installed directory you will find a file called `shared.html` - you need to host this on a common url that is passed to the initialization function.  You might be able to use this one if it's still there: `http://shared-localstorage.s3-website-eu-west-1.amazonaws.com/`, but you should really host it yourself somewhere.

Then firstly you need to initialize the system passing the URL:

```javascript

import {initialize} from "shared-storage";

initialize("http://shared-localstorage.s3-website-eu-west-1.amazonaws.com/");

```

Once you've done that you can use `setItem` and `getItem`, which return promises, to access the shared storage.

```javascript
import {initialize, setItem, getItem} from "shared-storage";

initialize("http://shared-localstorage.s3-website-eu-west-1.amazonaws.com/");

async function someFunction() {
    await setItem("test", someValue);
}

function someOtherFunction() {
    getItem("test", "default value").then(value=>console.log(value));
}

async function someRetrievalFunction() {
    let id = await getItem("user_id");
    if(id) {
        //...
    }
}

```
