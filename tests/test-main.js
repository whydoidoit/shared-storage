import {initialize, reset, setItem, getItem} from "../index.js";
import chai from "chai";

const expect = chai.expect;

describe('Remote local storage', () => {
    beforeEach(() => {
        reset();
    });
    it("should initialize", async () => {
        let window = await initialize();
        assert(window);
    });
    it("should fail to set/get if not initialized", async () => {
        let thrown = "";
        try {
            await setItem("test", "test");
        } catch (e) {
            thrown = e.message;
        }
        expect(thrown).to.equal("Not initialized");
        thrown = "";
        try {
            await getItem("test", "test");
        } catch (e) {
            thrown = e.message;
        }
        expect(thrown).to.equal("Not initialized");
    });
    it("should be able to set and get a value", async () => {
        await initialize();
        await setItem("test", "test");
        let result = await getItem("test", "banana");
        expect(result).to.equal("test");
    });
    it("should be able to get a default value", async ()=> {
        await initialize();
        await setItem("test", "test");
        let result = await getItem("test2", "banana");
        expect(result).to.equal("banana");
    });
    it("should be able to use a remotely hosted set of values", async ()=>{
        console.log("start");
        await initialize("http://shared-localstorage.s3-website-eu-west-1.amazonaws.com/");
        console.log("init");
        await setItem("test", "test");
        let result = await getItem("test", "banana");
        expect(result).to.equal("test");
    });
    it("should be able to use a remotely hosted set of values retained between refreshes", async () => {
        await initialize("http://shared-localstorage.s3-website-eu-west-1.amazonaws.com/");
        await setItem("test", "test");
        let result = await getItem("test", "banana");
        expect(result).to.equal("test");
        reset();
        await initialize("http://shared-localstorage.s3-website-eu-west-1.amazonaws.com/");
        result = await getItem("test", "banana");
        expect(result).to.equal("test");
    });
    it("should still have that last value!", async () => {
        await initialize("http://shared-localstorage.s3-website-eu-west-1.amazonaws.com/");
        let result = await getItem("test", "banana");
        expect(result).to.equal("test");
    });

});
