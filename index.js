let promise = null;
let standardPath = "/shared.html";
let contentWindow;
let usedPath;
let promises = {};
let id = 0;
let initializeResolver;

/* Handle responses */

window.addEventListener("message", event => {
    if (event.data.ready) {
        initializeResolver(contentWindow);
        return;
    }
    if(event.data.method !== "localStorage") return;
    let entry = promises[event.data.id];
    if(entry) {
        entry.resolve(event.data.result);
    }
    delete promises[event.data.id];
});

/* Inject a new frame with the remote window in it */
function initialize(path) {
    return promise || (promise = new Promise(function (resolve) {
        let iframe = document.createElement("iframe");
        iframe.src = path || standardPath;
        usedPath = iframe.src.startsWith("http") ? iframe.src : "*";
        iframe.style.position = "absolute";
        iframe.style.width = "0px";
        iframe.style.height = "0px";
        iframe.style.opacity = 0.01;
        document.body.appendChild(iframe);
        contentWindow = iframe.contentWindow;
        initializeResolver = resolve;
    }));
}

function setDefaultPath(path) {
    standardPath = path;
}

async function setItem(key, value) {
    if (!promise) throw new Error("Not initialized");
    await promise;
    contentWindow.postMessage({key, value, mode: "set", method: "localStorage"}, usedPath);
}

async function getItem(key, defaultValue) {
    if (!promise) throw new Error("Not initialized");
    await promise;
    let toSend = {id, key, value: null, defaultValue, mode: "get", method: "localStorage"};
    let entry = {};
    entry.promise = new Promise(function (resolve) {
        entry.resolve = resolve;
    });
    promises[id++] = entry;
    contentWindow.postMessage(toSend, usedPath);
    return entry.promise;
}

function reset() {
    promise = null;
    contentWindow = null;
}

export {initialize, setDefaultPath, setItem, getItem, reset};




